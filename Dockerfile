# Pull base image
FROM centos:7

# Install apache
RUN yum -y --setopt=tsflags=nodocs update && \
    yum -y --setopt=tsflags=nodocs install httpd && \
    yum clean all

# Copy index to document root
COPY index.html /var/www/html/index.html

# Expose HTTP Port
EXPOSE 80

# Run apachectl
ENTRYPOINT ["/usr/sbin/httpd"]
CMD ["-D", "FOREGROUND"]
